# kibanatops-alpine

[![](https://github.com/nu11secur1ty/elktops/blob/master/logo/ktops.png)](https://github.com/nu11secur1ty/elktops) 

[![](https://travis-ci.com/simple-icons/simple-icons-font.svg?branch=develop)](https://github.com/nu11secur1ty/elktops)

![](https://github.com/nu11secur1ty/elktops/blob/master/logo/OPS.PNG)

- run... `ELK 6.8.10`

```bash
docker run -d --name elstack -p 80:80 -p 9200:9200 nu11secur1ty/elk:6.8.10
```

- login to a docker

```bash
docker exec -it elstack bash
```
--------------------------------------------------------------------------
- - - Building procedure on Ubuntu 20.04

![](https://github.com/nu11secur1ty/elktops/blob/master/logo/buildprocedure.PNG)

# Stoping procedure

- listing docker machines

```bash
root@nu11secur1ty:~# docker images -a
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
nu11secur1ty/elk    6.8.10              406f893c5efb        2 hours ago         826MB
```
- stop docker

```bash
docker stop elstack
```
- remove docker machine
```bash
docker image rmi -f nu11secur1ty/elk:6.8.10
```
- for all machines
```bash
docker system prune -a
```

- - - commands in real-time... 
```yml
root@nu11secur1ty:~# docker ps -a
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                                                                     NAMES
595300eafb8d        406f893c5efb        "/sbin/tini -- /usr/…"   48 minutes ago      Up 48 minutes       443/tcp, 0.0.0.0:80->80/tcp, 5601/tcp, 0.0.0.0:9200->9200/tcp, 9300/tcp   elstack
root@nu11secur1ty:~# docker rm
rm   rmi  
root@nu11secur1ty:~# docker rm 595300eafb8d
Error response from daemon: You cannot remove a running container 595300eafb8df0a68717076c5bef144e41cb9f89326717536e75a0c445444408. Stop the container before attempting removal or force remove
root@nu11secur1ty:~# docker stop 595300eafb8d
595300eafb8d
root@nu11secur1ty:~# docker rm 595300eafb8d
595300eafb8d
```
# BR ;)
